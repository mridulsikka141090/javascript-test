/**
 * Bootstraps PriceCalculator by exposing its public API to provided context.
 * @param {Object} context Context to add module to
 */
(function(context) {
  "use strict";

  /**
   * Returns PriceCalculator's public API.
   * @return {Object} Public API
   */

  function priceCalculator() {
    var moduleInfo = {
      name: "Price Calculator",

      /**
       * Prints name of this module.
       * @param {Element} span DOM-lement to print name to
       */

      printName: function(span) {
        var self = this;
        span.textContent = "Will print module name in 3000ms: ";
        setTimeout(function timeoutHandler() {
          span.textContent += "> " + self.name;
        }, 3000);
        return self.name;
      }
    };

    // userType, 0 = normal, 1 = company
    // productType, 0 = new product, 1 = old product
    // price, the price of the product
    var calculatePrice = function(userType, productType, price, publishedDate) {
      var Userinfo = {
        normalUser: 0,
        companyUser: 1,
        productTypeIsNew: 0,
        productTypeisOld: 1,
        enddateDiscountOnSameDay: 10,
        enddateDiscountAfterPublishedDate: 0,
        rebateForCompanyUsers: 5,
        additionalFeeForNewProduct: 25,
        additionalFeeForOldProduct: 35,
        currentDate: new Date().toDateString(),
        priceCalculationFormula: function(Price, additionalFee, Rebate) {
          var result = Price + additionalFee - Rebate;
          return result;
        }
      };

      try {
        switch (
          userType // Usertype determines the new and company users
        ) {
          case Userinfo.normalUser: // normal User is 0
            switch (
              productType // Type of product
            ) {
              case Userinfo.productTypeIsNew: // New product value is 0
                if (publishedDate.toDateString() == Userinfo.currentDate) {
                  // Price Calculation Formula defined for calcalution
                  return Userinfo.priceCalculationFormula(
                    price,
                    Userinfo.additionalFeeForNewProduct,
                    Userinfo.enddateDiscountOnSameDay
                  );
                }
                break;
              case Userinfo.productTypeisOld: //Old product is 1
                return Userinfo.priceCalculationFormula(
                  price,
                  Userinfo.additionalFeeForOldProduct,
                  Userinfo.enddateDiscountAfterPublishedDate
                );
                break;
            }
            break;

          case Userinfo.companyUser: // company User value is 1
            switch (productType) {
              case Userinfo.productTypeIsNew: // New product value is 0
                if (publishedDate.toDateString() === Userinfo.currentDate) {
                  // Price Calculation Formula defined for calcalution
                  return Userinfo.priceCalculationFormula(
                    price,
                    Userinfo.additionalFeeForNewProduct,
                    Userinfo.rebateForCompanyUsers +
                      Userinfo.enddateDiscountOnSameDay
                  ); // Enddate discount and company discount
                } 
                else {
                  // Only company discount
                  return Userinfo.priceCalculationFormula(
                    price,
                    Userinfo.additionalFeeForNewProduct,
                    Userinfo.rebateForCompanyUsers
                  );
                }
                break;
              case Userinfo.productTypeisOld: //Old Product value is 1
                return Userinfo.priceCalculationFormula(
                  price,
                  Userinfo.additionalFeeForOldProduct,
                  Userinfo.rebateForCompanyUsers
                );
            }
            break;
        }
      } catch (ex) {
        console.log(ex);
      }
      return 0;
    };

    /**
     * Generates list of calculated prices.
     * @param {Element} priceContainer DOM-lement list prices in
     * @param {Array.<Number>} priceContainer List of prices to display
     */
    function generateList(priceContainer, prices) {
      var DELAY = 1000,
        price,
        i;
      function setTimeOutFunc(i, price, priceCounter) {
        setTimeout(function() {
          var paragraph = document.createElement("p");
          paragraph.innerHTML =
            "Calculated price for item #" + priceCounter + " is: " + price;
          priceContainer.appendChild(paragraph);
        }, DELAY + DELAY * i);
      }

      for (var i = 0; i < prices.length; i += 1) {
        var priceCounter = i + 1;
        var price = prices[i];
        setTimeOutFunc(i, price, priceCounter);
      }
    }

    /**
     * Recursively calculates factorial value.
     * @param {Integer} value Value to calculate factorial of
     * @return {Integer} Factorial value
     */
    var getFactorial = function(value) {
      // TODO: Write function body that recursively calculates factorial of provided value
      var factorialResult = value;
      if (value == 0 || value == 1) {
        return 1;
      }
      while (value > 1) {
        --value;
        factorialResult = factorialResult * value;
      }
      return factorialResult;
    };

    // Public API
    return {
      calculatePrice: calculatePrice,
      calculateFactorial: getFactorial,
      displayPrices: function(prices) {
        generateList(document.querySelector(".price-container"), prices);
      },
      printName: function() {
        return moduleInfo.printName(document.querySelector(".nameInfo"));
      },
      /**
       * Calculates factorial value.
       * @param {Integer} value Value to calculate factorial of
       */
      calcFactorial: function(value) {
        var factorialText = "";

        for (var i = 0; i < value; i += 1) {
          factorialText += i + 1 + (i + 1 < value ? "*" : "");
        }

        document.querySelector(".factorial").textContent =
          "Factorial of !" +
          value +
          " (" +
          factorialText +
          ") = " +
          getFactorial(value);
      }
    };
  }

  context.PriceCalculator = priceCalculator();
})(window);
