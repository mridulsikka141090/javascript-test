describe('price calculator', function(){
  it('should calc price right equal to 16 (UserType: 0 , ProductType: 0 , Price: 1 , publishedDate: New Date)', function(){
    var userType = 0;
    var productType = 0;
    var price = 1;
    var publishedDate = new Date();

    var expected = 16;
    var actual = PriceCalculator.calculatePrice(userType, productType, price, publishedDate);
    expect(expected).to.equal(actual);
  });
  it('should calc price right  equal to 45 (UserType: 0 , ProductType: 1 , Price: 10 , Published Date : Yesterday)', function(){
    var userType = 0;
    var productType = 1;
    var price = 10;
    var publishedDate = new DateDate.now() - 864e5();

    var expected = 45;
    var actual = PriceCalculator.calculatePrice(userType, productType, price, publishedDate);
    expect(expected).to.equal(actual);
  });
  it('should calc price right equal to 30 (UserType: 1 , ProductType: 0 , Price: 20 , PublishedDate : New Date)', function(){
    var userType = 1;
    var productType = 0;
    var price = 20;
    var publishedDate = new Date();

    var expected = 30;
    var actual = PriceCalculator.calculatePrice(userType, productType, price, publishedDate);
    expect(expected).to.equal(actual);
  });
  it('should calc price right equal to 120 (UserType: 1 , ProductType: 0 , Price: 100 , published Date : Yesterday)', function(){
    var userType = 1;
    var productType = 0;
    var price = 100;
    var publishedDate = new Date(Date.now() - 864e5);

    var expected = 120;
    var actual = PriceCalculator.calculatePrice(userType, productType, price, publishedDate);
    expect(expected).to.equal(actual);
  });

  it('should calc price right equal to 130 (UserType: 1 , ProductType: 1 , Price: 100 , published Date : does not matter)', function(){
    var userType = 1;
    var productType = 1;
    var price = 100;
    var publishedDate = new Date();

    var expected = 130;
    var actual = PriceCalculator.calculatePrice(userType, productType, price, publishedDate);
    expect(expected).to.equal(actual);
  });
});

describe('Get Factorial Value', function(){
  it('if value is given 0 then factorial result should be 1', function (){
    var value = 0;
    var expectedFactorial = 1;

    var valueFact = PriceCalculator.calculateFactorial(value);
    expect(expectedFactorial).to.equal(valueFact);
  });

  it('if value is given 5 then factorial result should be 120', function (){
    var value = 5;
    var expectedFactorial = 120;

    var valueFact = PriceCalculator.calculateFactorial(value);
    expect(expectedFactorial).to.equal(valueFact);
  });
});



